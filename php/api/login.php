<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require "../vendor/autoload.php";
use \Firebase\JWT\JWT;


$input = json_decode(file_get_contents("php://input"));

$uname 		= 'test';;
$uname 		= $input->username;
//$password 	= $_POST['password'];

$conn = mysqli_connect("localhost","root","","test");
$sql = "select id, uname from users where uname = '".$uname."'";
$res = mysqli_query($conn,$sql);
if(mysqli_num_rows($res)>0)
{
while($row = mysqli_fetch_array($res)){
	$val['id'] 		= $row['id'];
	$val['uname'] 	= $row['uname'];


	$secret_key 		= "YOUR_SECRET_KEY";
	$issuer_claim 		= "THE_ISSUER";
	$audience_claim 	= "THE_AUDIENCE";
	$issuedat_claim 	= time(); // issued at
	$notbefore_claim 	= time(); //not before
	$token = array(
		"iss" => $issuer_claim,
		"aud" => $audience_claim,
		"iat" => $issuedat_claim,
		"nbf" => $notbefore_claim,
		"data" => $val
	);
	
	http_response_code(200);
	$jwt = JWT::encode($token, $secret_key);
	
	echo json_encode(
		array(
			"message" 		=> "Successful login.",
			"expireAt"		=> time(),
			"user_details"	=> $val,
			"jwt" 			=> $jwt
		));
}
}
else{
	echo json_encode(
		array(
			"message" 		=> "Login failed.",
			"expireAt" 		=> "",
			"jwt" 			=> ""
		));	
}
?>