import React, { Component }  from 'react';
import axios from 'axios';
import cors from 'cors';
import './App.css';
 
class Second extends React.Component {  
	constructor() {  
		super();
		this.state 			= {rand:985,isLoading:7814521452,comment:785,username:'test',email:'test01',password:'test',};
		this.frmSubmit 		= this.frmSubmit.bind(this);
		this.updateRand 	= this.updateRand.bind(this);
		this.getToken 		= this.getToken.bind(this);
		this.getUser 		= this.getUser.bind(this);
		this.removeToken 	= this.removeToken.bind(this);
		//this.updateUser 	= this.updateUser.bind(this);
	}
	onChange = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	}

	removeToken = () => {
		localStorage.setItem("access_token", '146');
		localStorage.setItem("expire_at",0);
		console.log('--------------')
		//localStorage.removeItem("access_token");
		//localStorage.removeItem("expire_at");
	}

	updateRand(event){
		this.setState({comment: 'Hello'});
	}

	getToken(){
		console.log(localStorage.getItem("access_token"));
		
	}

	getUser(event){
		event.preventDefault();
		console.log('-------------------'+localStorage.getItem("access_token"));
		fetch('http://localhost/projects/reactjs/app03/php/api/protected.php', {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': localStorage.getItem("access_token"),
				'Host': 'api.producthunt.com'
			}			
		})
		.then(response => response.json())
		.then(
			console.log(777)
		)
		.catch(error => this.setState({ isLoading: false }));
	}
	
	frmSubmit(event) {
		event.preventDefault();
		
		console.log(this.state.username);
		
		var apiurl = '';
		const data = new FormData(event.target);
		
		
        const { username, email } = this.state;
		
		console.log(event);
		fetch('http://localhost/projects/reactjs/app03/php/api/login.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + this.state.access_token,				
			},
			body: JSON.stringify(this.state),
		})
		.then(response => response.json())
		.then(
			(myresult) => {
				localStorage.setItem("access_token", myresult.jwt);
				this.setState({isLoading: myresult.expireAt+'__'+myresult.jwt});
			}
		)
		.catch(error => this.setState({ isLoading: false }));
		this.getToken();
	}
	
	render() {  
		return (
			<div style={{border:'1px solid #FFF', margin:'10px', padding:'10px'}}>
				<ul>
					<li>{this.state.rand}</li>
					<li>{this.state.comment}</li>
					<li>{this.state.isLoading}</li>
					<li>{localStorage.getItem("access_token")}</li>
				</ul>
				
				<form onSubmit={this.frmSubmit}>
					<table>
						<tbody>
							<tr>
								<td>Enter username </td>
								<td><input id="username" name="username" type="text" value={this.state.username} onChange={this.onChange} /></td>
							</tr>
							<tr>
								<td>Enter your email</td>
								<td><input id="email" name="email" type="text" value={this.state.email} onChange={this.onChange} /></td>
							</tr>
							<tr>
								<td>Enter your password</td>
								<td><input id="password" name="password" type="password" value={this.state.password} onChange={this.onChange} /></td>
							</tr>
							<tr>
								<td>Enter your birth date</td>
								<td><input id="birthdate" name="birthdate" type="date" /></td>
							</tr>
							<tr>
								<td></td>
								<td>
									<button type="button" onClick={this.updateRand}>Update data!</button> &nbsp; &nbsp;
									<button type="button" onClick={this.getToken}>Show Token!</button> &nbsp; &nbsp;
									<button type="button" onClick={this.removeToken}>Remove Token!</button> &nbsp; &nbsp;
									<button type="button" onClick={this.getUser}>Get User</button> &nbsp; &nbsp;
									<button>Send data!</button>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
				
			</div>
		);  
   }  
}  
export default Second;