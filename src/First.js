import React, { Component }  from 'react';
import './App.css';
 
class First extends React.Component {  
	constructor() {  
		super();
		this.state = {  
			rand:75,
			data:   
			[  
				{             
					"name":"Abhishek"             
				},  
				{            
					"name":"Saharsh"             
				},  
				{    
					"name":"Ajay"          
				}  
			]  
		}  
	} 
	render() {  
		return (
			<div style={{border:'1px solid #FFF', marginTop:'10px'}}>
				<ul>
					<li>{this.state.rand}</li>
					{this.state.data.map((item) => <li key={item.name}>{item.name}</li>)}           
				</ul>
			</div>
		);  
   }  
}  
export default First;